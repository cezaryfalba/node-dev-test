// tslint:disable: no-var-requires

import { OrderMapper } from '../../Order/Service/OrderMapper';
import { Repository } from '../../Order/Service/Repository';

import { IBestSellers, IBestBuyers } from '../Model/IReports';

import { ReportController } from './ReportController';

const ORDER_MOCK = require('../../Order/Resources/Data/orders');

describe('ReportController', () => {
  let reportController: ReportController;
  let orderMapper: OrderMapper;

  beforeEach(() => {
    orderMapper = new OrderMapper();
    orderMapper.repository = new Repository();
    reportController = new ReportController(orderMapper);
  });

  describe('Best Sellers', () => {
    let res: IBestSellers[];

    beforeEach(async() => {
        res = await reportController.bestSellers(ORDER_MOCK[0].createdAt);
    });

    it('should return best sellers', () => {
        expect(res).toBeTruthy();
    });

    it('should return only one best seller', () => {
        expect(res.length).toBe(1);
    });

    it('should return proper values for the best seller', () => {
        expect(res[0].productName).toBe('Black sport shoes');
        expect(res[0].quantity).toBe(2);
        expect(res[0].totalPrice).toBe(220);
    });
  });

  describe('Best Buyers', () => {
    let res: IBestBuyers;

    beforeEach(async() => {
        res = await reportController.bestBuyers(ORDER_MOCK[0].createdAt);
    });

    it('should return best buyers', () => {
        expect(res).toBeTruthy();
    });

    it('should return proper values for the best buyer', () => {
        expect(res.customerName).toBe('John Doe');
        expect(res.totalPrice).toBe(135.75);
    });
  });
});
