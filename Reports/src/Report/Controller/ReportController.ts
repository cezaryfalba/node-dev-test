import { Controller, Get, Param } from '@nestjs/common';
import { OrderMapper } from '../../Order/Service/OrderMapper';

import { IBestSellers, IBestBuyers } from '../Model/IReports';

@Controller()
export class ReportController {
  constructor(private orderMapper: OrderMapper) {}

  @Get('/report/products/:date')
  async bestSellers(@Param('date') date: string) {
    const productsSales = [];

    for (const order of await this.orderMapper.repository.fetchOrdersForDate(date)) {
      for (const product of order.products as number[]) {
        const productSales = productsSales.find(p => p.id === product);

        if (productSales) {
          productSales.sales++;
        } else {
          productsSales.push({
            id: product,
            sales: 1,
          });
        }
      }
    }

    const maxSales = Math.max.apply(Math, productsSales.map(p => p.sales));

    return await Promise.all(productsSales.filter(p => p.sales === maxSales).map(p => this.orderMapper.repository.fetchProduct(p.id)))
      .then(products => products.map(product => ( {
        productName: product.name,
        quantity: maxSales,
        totalPrice: maxSales * product.price,
      } as IBestSellers)));
  }

  @Get('/report/customer/:date')
  async bestBuyers(@Param('date') date: string) {
    const customerSpendings = [];

    for (const order of await this.orderMapper.repository.fetchOrdersForDate(date)) {
      const orderTotal = await Promise.all(order.products.map(id => this.orderMapper.repository.fetchProduct(id)))
        .then(products => products.map(product => product.price).reduce((a, b) => a + b, 0));

      const customerSpending = customerSpendings.find(c => c.id === order.customer);

      if (customerSpending) {
        customerSpending.spent += orderTotal;
      } else {
        customerSpendings.push({
          id: order.customer,
          spent: orderTotal,
        });
      }
    }

    const bestBuyer = customerSpendings.sort((a, b) => a.spent > b.spent ? 1 : -1).pop();
    const bestCustomer = await this.orderMapper.repository.fetchCustomer(bestBuyer.id);

    return {
      customerName: `${bestCustomer.firstName} ${bestCustomer.lastName}`,
      totalPrice: bestBuyer.spent,
    } as IBestBuyers;
  }
}
