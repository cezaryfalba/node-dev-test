import { Injectable } from '@nestjs/common';

import { Customer } from '../Model/Customer';
import { Order } from '../Model/Order';
import { Product } from '../Model/Product';

/**
 * Data layer - mocked
 */
@Injectable()
export class Repository {
  fetchOrders(): Promise<Order[]> {
    return new Promise(resolve => resolve(require('../Resources/Data/orders')));
  }

  fetchOrdersForDate(date: string): Promise<Order[]> {
    return this.fetchOrders().then(orders => orders.filter(o => o.createdAt === date));
  }

  fetchProducts(): Promise<Product[]> {
    return new Promise(resolve =>
      resolve(require('../Resources/Data/products')),
    );
  }

  fetchProduct(id: number): Promise<Product> {
    return this.fetchProducts().then(products => products.find(p => p.id === id));
  }

  fetchCustomers(): Promise<Customer[]> {
    return new Promise(resolve =>
      resolve(require('../Resources/Data/customers')),
    );
  }

  fetchCustomer(id: number): Promise<Customer> {
    return this.fetchCustomers().then(customers => customers.find(c => c.id === id));
  }
}
