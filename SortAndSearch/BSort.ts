import { BSearch } from './BSearch';

export class BSort {
    // to make use of previously written binary search and for naming consistency, let's implement binary sort

    sorted: number[];

    constructor(array: number[]) {
        this.sorted = array;

        for (let a = 1; a < this.sorted.length; ++a) {
            let b = a - 1;

            const selected = this.sorted[a];

            const location = BSearch.getInstance().search(this.sorted, selected, 0, b);

            if (location >= 0) {
                while(b >= location) {
                    this.sorted[b + 1] = this.sorted[b];
                    b--;
                }

                this.sorted[b + 1] = selected;
            }

        }
    }
}
