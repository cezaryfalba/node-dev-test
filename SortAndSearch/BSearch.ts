export class BSearch {
    private static instance: BSearch;

    private _operations: number = 0;
    get operations(): number {
        return this._operations;
    }

    public static getInstance() {
        if (!BSearch.instance) {
            BSearch.instance = new BSearch();
        }

        return BSearch.instance;
    }

    public search(array: number[], search: number, left?: number, right?: number): number {
        this._operations++;

        if (!left && !right) {
            left = 0;
            right = array.length -1;
        }

        if (right >= left) {
            const mid = Math.floor(left + (right - left) / 2);

            if (array[mid] === search) {
                return mid;
            }

            if (array[mid] > search) {
                return this.search(array, search, left, mid - 1);
            } else {
                return this.search(array, search, mid + 1, right);
            }
        }

        return -1;
    }
}
