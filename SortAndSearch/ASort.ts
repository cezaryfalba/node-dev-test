export class ASort {
    // simple bubble sort implementation

    sorted: number[];

    constructor(array: number[]) {
        this.sorted = array;

        for (let a = 0; a < this.sorted.length - 1; a++) {
            for (let b = 0; b < this.sorted.length - a - 1; b++) {
                if (this.sorted[b] > this.sorted[b + 1]) {
                    const t = array[b];

                    this.sorted[b] = this.sorted[b + 1];
                    this.sorted[b + 1] = t;
                }
            }
        }
    }
}
