import { ASort } from './ASort';
import { BSort } from './BSort';
import { BSearch } from './BSearch';

const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
const elementsToFind = [1, 5, 13, 27, 77];

const sorted: number[] = new ASort(unsorted).sorted;

elementsToFind.map(e => {
    const index = BSearch.getInstance().search(sorted, e);

    console.log(
        e,
        index === -1 ? 'has not been found' : 'has been found at:',
        index === -1 ? '' : index
    );
});

console.log('BSearch operations count:', BSearch.getInstance().operations);

console.log('sorted using ASort (bubble sorting algorithm):', sorted);
console.log('sorted using BSort (binary insertion sorting algorithm):', new BSort(unsorted).sorted);
